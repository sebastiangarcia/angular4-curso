# Angular 4

Este proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) versión 1.2.7.

## Cómo utilizo las apps?

Para utilizar las apps, deben ingresar a la carpeta de esta y luego ejecutar:

	npm install

## Deploy con Maven

Para realizar un deploy utilizando Maven, pueden utilizar el plugin: [frontend-maven-plugin](https://github.com/eirslett/frontend-maven-plugin) el cual permitirá ejecutar comandos NPM. En particular deberán ejecutar el siguiente comando:

	npm run ng -- build --prod
	
Luego deben copiar el contenido de la carpeta *./dist* al la carpeta que alojará el contenido web del servidor que estén utilizando. Para ello pueden utilizar 2 alternativas:

1. Plugin maven: [maven-resources-plugin](https://maven.apache.org/plugins/maven-resources-plugin/)
2. Utilizar el flag *--output-path* de ng build

Para el último caso el comando quedaría como:

	npm run ng -- build --prod --output-path /path/to/webRoot

Por último, recuerden siempre tener en consideración el base href:

	npm run ng -- build --prod --output-path /path/to/webRoot --base-href /mi/ruta/despues/del/dominio


## Servidor Desarrollo

Ejecutar `ng serve` para un nuevo servidor de desarrollo. Navega a `http://localhost:4200/`. La app se actualizará automáticamente con cada cambio en las fuentes.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).