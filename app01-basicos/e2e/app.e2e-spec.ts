import { App01BasicosPage } from './app.po';

describe('app01-basicos App', () => {
  let page: App01BasicosPage;

  beforeEach(() => {
    page = new App01BasicosPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
