import { Component } from '@angular/core';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styles: [`
    .online {
      color: white;
    }
  `]
})
export class ServerComponent {
  serverId: number = 10;
  serverStatus: string = 'fuera de línea';

  constructor() {
    this.serverStatus = Math.random() > 0.5 ? 'en línea' : 'fuera de línea';
  }

  getServerStatus() {
    return this.serverStatus;
  }

  getColor() {
    return this.serverStatus === 'en línea' ? 'green' : 'red';
  }
}
