import {Component, OnChanges, OnInit} from '@angular/core';

@Component({
  // selector: '[app-servers]',
  // selector: '.app-servers',
  selector: 'app-servers',
  // template: `
  //   <app-server></app-server>
  //   <app-server></app-server>`,
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit, OnChanges {
  allowNewServer = false;
  serverCreationStatus = 'El servidor NO fue creado!';
  serverName = 'SVR_';
  serverCreated = false;
  servers = ['SVR_Testserver', 'SVR_Testserver 2'];

  constructor() {
    setTimeout(() => {
      this.allowNewServer = true;
    }, 2000);
  }

  ngOnInit() {
    console.log('ServersComponent acaba de iniciar!');
  }

  ngOnChanges() {
    console.log('Algo cambió en ServersComponents');
  }

  onCreateServer() {
    this.serverCreated = true;
    this.servers.push(this.serverName);
    this.serverCreationStatus = 'El servidor fue creado! Su nombre es ' + this.serverName;
  }

  onUpdateServerName(event: Event) {
    this.serverName = (<HTMLInputElement>event.target).value;
  }
}
