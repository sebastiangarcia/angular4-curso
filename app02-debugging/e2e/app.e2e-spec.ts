import { App02DebuggingPage } from './app.po';

describe('app02-debugging App', () => {
  let page: App02DebuggingPage;

  beforeEach(() => {
    page = new App02DebuggingPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
