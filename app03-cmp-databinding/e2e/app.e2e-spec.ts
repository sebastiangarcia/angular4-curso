import { App03CmpDatabindingPage } from './app.po';

describe('app03-cmp-databinding App', () => {
  let page: App03CmpDatabindingPage;

  beforeEach(() => {
    page = new App03CmpDatabindingPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
