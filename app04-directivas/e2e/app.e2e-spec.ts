import { App04DirectivasPage } from './app.po';

describe('app04-directivas App', () => {
  let page: App04DirectivasPage;

  beforeEach(() => {
    page = new App04DirectivasPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
