import { App05ServiciosPage } from './app.po';

describe('app05-servicios App', () => {
  let page: App05ServiciosPage;

  beforeEach(() => {
    page = new App05ServiciosPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
