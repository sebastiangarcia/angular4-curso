import { App06RuteoPage } from './app.po';

describe('app06-ruteo App', () => {
  let page: App06RuteoPage;

  beforeEach(() => {
    page = new App06RuteoPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
