import {Http, Response} from '@angular/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class AuthService {
  public token: string;
  loggedIn = false;

  constructor(private http: Http) {}

  isAuthenticated() {
    const promise = new Promise(
      (resolve, reject) => {
        setTimeout(() => {
          resolve(this.loggedIn);
        }, 100);
      }
    );
    return promise;
  }

  login() {
    this.http.post('https://demo4869696.mockable.io/login', {username: 'sgarcia', passwd: '123456'})
      .map(
        (response: Response) => {
          return response.json();
          }
        )
      .catch(
          (error: Response) => {
            console.log(error);
            return Observable.throw(error.json().error);
          }
        )
      .subscribe(
        (data: any) => {
          this.token = data.token;
          this.loggedIn = true;
          console.log(this.token);
        },
        (error: any) => {
          console.log('Error Login: ' + error);
        }
      );
    // this.loggedIn = true;
  }

  logout() {
    this.loggedIn = false;
  }
}
