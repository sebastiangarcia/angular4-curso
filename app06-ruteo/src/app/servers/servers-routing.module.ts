import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditServerComponent } from './edit-server/edit-server.component';
import { ServerComponent } from './server/server.component';
import { ServersComponent } from './servers.component';
import { AuthGuard } from '../auth-guard.service';
import { CanDeactivateGuard } from './edit-server/can-deactivate-guard.service';
import { ServerResolver } from './server/server-resolver.service';

const appRoutes: Routes = [
  {
    path: '',
    // canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: ServersComponent,
    children: [
    { path: ':id', component: ServerComponent, resolve: {server: ServerResolver} },
    { path: ':id/edit', component: EditServerComponent, canDeactivate: [CanDeactivateGuard] }
  ] }
];

@NgModule({
  imports: [
    // RouterModule.forRoot(appRoutes, {useHash: true})
    RouterModule.forChild(appRoutes)
  ],
  exports: [RouterModule]
})
export class ServersRoutingModule {

}
