import { Component, OnInit } from '@angular/core';
import { ServersService } from './servers.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  public servers: {id: number, name: string, status: string}[] = [];

  constructor(private serversService: ServersService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.serversService.getServers().subscribe(
      (data: any) => {
        this.servers = data;
      },
      (error: any) => console.log(error),
      () => console.log('Se cargaron los servidores')
    );
  }

  onReload() {
    // this.router.navigate(['servers'], {relativeTo: this.route});
  }

}
