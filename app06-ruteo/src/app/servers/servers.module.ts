import {NgModule} from '@angular/core';

import { ServersComponent } from './servers.component';
import { EditServerComponent } from './edit-server/edit-server.component';
import { ServerComponent } from './server/server.component';
import { ServersService } from './servers.service';
import { ServerResolver } from './server/server-resolver.service';
import {ServersRoutingModule} from './servers-routing.module';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [
    ServersComponent,
    EditServerComponent,
    ServerComponent
  ],
  imports: [
    ServersRoutingModule,
    CommonModule,
    FormsModule,
    SharedModule
  ],
  providers: [ServersService, ServerResolver]
})
export class ServersModule{

}
