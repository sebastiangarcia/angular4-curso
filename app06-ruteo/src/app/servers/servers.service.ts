import {Observable} from 'rxjs/Observable';
import {Http, Response} from '@angular/http';
import 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {AuthService} from '../auth.service';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class ServersService {
  private servers = [
    {
      id: 1,
      name: 'Productionserver',
      status: 'online'
    },
    {
      id: 2,
      name: 'Testserver',
      status: 'offline'
    },
    {
      id: 3,
      name: 'Devserver',
      status: 'offline'
    }
  ];

  constructor(private authService: AuthService, private http: Http) {}

  getServers() {
    /*let params: URLSearchParams = new URLSearchParams();
    params.set('token', this.authService.token);*/
    let params = new HttpParams();
    params = params.append('token', this.authService.token);
    return this.http.get('https://demo4869696.mockable.io/servers', {params: params} )
      .map(
        (response: Response) => {

          const data = response.json();
          for (const server of data) {
            server.name = 'API_' + server.name;
          }
          return data;
        }
      )
      .catch(
        (error: Response) => {
          console.log(error);
          return Observable.throw('Something went wrong');
        }
      );
  }

  getServer(id: number) {
    const server = this.servers.find(
      (s) => {
        return s.id === id;
      }
    );
    return server;
  }

  updateServer(id: number, serverInfo: {name: string, status: string}) {
    const server = this.servers.find(
      (s) => {
        return s.id === id;
      }
    );
    if (server) {
      server.name = serverInfo.name;
      server.status = serverInfo.status;
    }
  }
}
