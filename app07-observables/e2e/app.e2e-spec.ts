import { App07ObservablesPage } from './app.po';

describe('app07-observables App', () => {
  let page: App07ObservablesPage;

  beforeEach(() => {
    page = new App07ObservablesPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
