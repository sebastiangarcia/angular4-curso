import { App08FormulariosPage } from './app.po';

describe('app08-formularios App', () => {
  let page: App08FormulariosPage;

  beforeEach(() => {
    page = new App08FormulariosPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
