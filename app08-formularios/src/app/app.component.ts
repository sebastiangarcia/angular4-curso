import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {NgForm, NgModel} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('f') signupForm: NgForm;
  @ViewChild('email') elementoEmail: NgModel;
  defaultQuestion = 'comida';
  answer = '';
  genders = ['male', 'female'];
  user = {
    username: '',
    email: '',
    secretQuestion: '',
    answer: '',
    gender: ''
  };
  submitted = false;
  preguntas: {key: string, value: string}[] = [
    {key: 'color', value: 'Tu color favorito?'},
    {key: 'comida', value: 'Tu comida favorita?' }
  ];

  suggestUserName() {
    const suggestedName = 'Superuser';
    // this.signupForm.setValue({
    //   userData: {
    //     username: suggestedName,
    //     email: ''
    //   },
    //   secret: 'pet',
    //   questionAnswer: '',
    //   gender: 'male'
    // });
    this.signupForm.form.patchValue({
      userData: {
        username: suggestedName,
        email: 'sebastian@wildit.cl'
      },
      gender: 'male'
    });
  }

  // onSubmit(form: NgForm) {
  //   console.log(form);
  // }

  onChangeUsername(username: NgModel){

  }

  onSubmit(f: NgForm) {
    console.log(this.signupForm);
    if (f.valid) {
      this.submitted = true;
      this.user.username = f.value.userData.username;
      this.user.email = this.elementoEmail.value;
      this.user.secretQuestion = this.signupForm.value.secret;
      this.user.answer = this.signupForm.value.questionAnswer;
      this.user.gender = this.signupForm.controls.gender.value;

      this.signupForm.reset();
    }
  }

  ngOnInit() {
    console.log(this.signupForm);
  }
}
