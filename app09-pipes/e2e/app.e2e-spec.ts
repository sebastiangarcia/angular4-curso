import { App09PipesPage } from './app.po';

describe('app09-pipes App', () => {
  let page: App09PipesPage;

  beforeEach(() => {
    page = new App09PipesPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
