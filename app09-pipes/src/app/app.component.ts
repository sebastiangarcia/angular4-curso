import { Component, OnInit } from '@angular/core';
import { EjemploPipe } from './pipes/ejemplo.pipe';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private ejemploPipe: EjemploPipe){}

  ngOnInit(){
    console.log(this.ejemploPipe.transform('Curso Angular',4));
  }

  appStatus = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('stable');
    }, 2000);
  });
  servers = [
    {
      instanceType: 'medium',
      name: 'Production',
      status: 'stable',
      started: new Date()
    },
    {
      instanceType: 'large',
      name: 'User Database',
      status: 'stable',
      started: new Date()
    },
    {
      instanceType: 'small',
      name: 'Development Server',
      status: 'offline',
      started: new Date()
    },
    {
      instanceType: 'small',
      name: 'Testing Environment Server',
      status: 'stable',
      started: new Date()
    }
  ];
  filteredStatus = '';
  getStatusClasses(server: {instanceType: string, name: string, status: string, started: Date}) {
    return {
      'list-group-item-success': server.status === 'stable',
      'list-group-item-warning': server.status === 'offline',
      'list-group-item-danger': server.status === 'critical'
    };
  }
  onAddServer() {
    this.servers.push({
      instanceType: 'small',
      name: 'New Server',
      status: 'stable',
      started: new Date(15, 1, 2017)
    });
  }
}
