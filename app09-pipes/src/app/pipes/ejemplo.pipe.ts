import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ejemplo'
})
export class EjemploPipe implements PipeTransform {

  transform(value: any, version: number): any {
    return value + ' ' + version;
  }

}
