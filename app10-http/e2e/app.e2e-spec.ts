import { App10HttpPage } from './app.po';

describe('app10-http App', () => {
  let page: App10HttpPage;

  beforeEach(() => {
    page = new App10HttpPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
