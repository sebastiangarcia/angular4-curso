import {Component, OnInit} from '@angular/core';
import { Response } from '@angular/http';
import { environment } from '../environments/environment';

import { ServerService } from './server.service';
import { ServerModelo } from './modelos/server.modelo';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  appName = this.serverService.getAppName();
  server1: ServerModelo = new ServerModelo(this.serverService, {'name': 'Testserver', 'capacity': 10, 'id': this.generateId()});
  server2: ServerModelo = new ServerModelo(this.serverService);
  servers: ServerModelo[] = [];

  /*servers: any[] = [
    {
      name: 'Testserver',
      capacity: 10,
      id: this.generateId()
    },
    {
      name: 'Liveserver',
      capacity: 100,
      id: this.generateId()
    }
  ];*/

  constructor(private serverService: ServerService) {
    /*this.server1.name = 'Testserver';
    this.server1.capacity = 10;
    this.server1.id = this.generateId();*/
    this.servers.push(this.server1);

    this.server2.name = 'Liveserver';
    this.server2.capacity = 10;
    this.server2.id = this.generateId();
    this.servers.push(this.server2);
  }

  ngOnInit() {
    console.log('MI Variable API es: '+environment.api);
    this.serverService.getAppName().subscribe(
      ( response: any ) => {
        console.log(response);
      }
    );
  }

  onAddServer(name: string) {
    let server = new ServerModelo(this.serverService);
    server.name = name;
    server.capacity = 50;
    server.id = this.generateId();

    this.servers.push(server);
  }
  onSave() {
    this.serverService.storeServers(this.servers)
      .subscribe(
        (response) => {
          console.log(response.text());
          console.log(response.json());
        },
        (error) => console.log(error),
        () => console.log('Se actualizaron los servidores!')
      );
  }
  onGet() {
    this.serverService.getServers()
      .subscribe(
        (servers: any[]) => this.servers = servers,
        (error) => console.log(error),
        () => console.log('Se cargaron los servidores')
      );
  }
  private generateId() {
    return Math.round(Math.random() * 10000);
  }
}
