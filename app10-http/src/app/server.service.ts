import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import {ServerModelo} from './modelos/server.modelo';

@Injectable()
export class ServerService {

  constructor(private http: Http) {}

  storeServers(servers: ServerModelo[]) {
    const headers = new Headers({'Content-Type': 'application/json', 'Mi-Header': 'Mi-Valor'});
    // return this.http.post('https://demo4869696.mockable.io/servers',
    //   servers,
    //   {headers: headers});
    return this.http.put('https://demo4869696.mockable.io/servers', servers, {headers: headers});
  }

  getServers() {
    return this.http.get('https://demo4869696.mockable.io/servers' )
      .map(
        (response: Response) => {

          const data = response.json();
          for (const server of data) {
            server.name = 'API_' + server.name;
          }
          return data;
        }
      )
      .catch(
        (error: Response) => {
          console.log(error);
          return Observable.throw('Something went wrong');
        }
      );
  }

  getAppName() {
    return this.http.get('http://demo4869696.mockable.io/appname')
      .map(
        (response: Response) => {
          return response.json().data;
        }
      );
  }
}
