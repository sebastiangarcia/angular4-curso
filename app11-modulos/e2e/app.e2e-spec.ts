import { App11ModulosPage } from './app.po';

describe('app11-modulos App', () => {
  let page: App11ModulosPage;

  beforeEach(() => {
    page = new App11ModulosPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
