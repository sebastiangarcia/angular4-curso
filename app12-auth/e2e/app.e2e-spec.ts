import { App12AuthPage } from './app.po';

describe('app12-auth App', () => {
  let page: App12AuthPage;

  beforeEach(() => {
    page = new App12AuthPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
