import { App13TestingPage } from './app.po';

describe('app13-testing App', () => {
  let page: App13TestingPage;

  beforeEach(() => {
    page = new App13TestingPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
