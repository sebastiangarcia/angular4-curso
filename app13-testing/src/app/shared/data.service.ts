export class DataService {
  getDetails() {
    const resultPromise = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve('Data');
      }, 1500);
    });
    return resultPromise;
  }
  getAsyncNow() {
    const resultPromise = new Promise((resolve, reject) => {
      //resolve('es Viernes!');
      setTimeout(() => {
        resolve('es Viernes!');
      }, 10);
    });
    return resultPromise;
  }
}
