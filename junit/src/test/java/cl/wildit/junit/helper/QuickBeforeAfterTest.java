package cl.wildit.junit.helper;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class QuickBeforeAfterTest {
	
	@BeforeClass
	public static void beforeClass() {
		System.out.println("Antes la Clase");
	}
	
	@Before
	public void before() {
		System.out.println("Antes de las Pruebas");
	}

	@Test
	public void test1() {
		System.out.println("test1 ejecutado");
	}
	
	@Test
	public void test2() {
		System.out.println("test2 ejecutado");
	}
	
	@After
	public void after() {
		System.out.println("Después de las Pruebas");
	}
	
	@AfterClass
	public static void afterClass() {
		System.out.println("Después la Clase");
	}
	
	// Instanciar helper de StringHelperTest con Before!

}
