package cl.wildit.junit.helper;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class StringHelperTest {
	
	StringHelper helper = new StringHelper();
	
	/*@BeforeClass
	public static void beforeClass() {
		StringHelperTest.helper = new StringHelper();
	}
	
	@Before
	public void before() {
		this.helper = new StringHelper();
	}*/

	/*@Test
	public void testTruncateAInFirst2Positions() {
		// fail("Not yet implemented");
		
		StringHelper helper = new StringHelper();
	
		assertEquals("CD", helper.truncateAInFirst2Positions("AACD"));
		//assertEquals(helper.truncateAInFirst2Positions("ACD"), "CD");
		//assertEquals(helper.truncateAInFirst2Positions("CDEF"), "CDEF");
		//assertEquals(helper.truncateAInFirst2Positions("CDAA"), "CDAA");
	}*/
	
	@Test
	public void testTruncateAInFirst2Positions_AinFirst2Positions() {
		assertEquals("CD", helper.truncateAInFirst2Positions("AACD"));
	}
	
	@Test
	public void testTruncateAInFirst2Positions_AinFirstPositions() {
		assertEquals("CD", helper.truncateAInFirst2Positions("ACD"));
	}
	
	@Test
	public void testAreFirstAndLastTwoCharactersTheSame_Basic_Negative() {
		//assertEquals(false, helper.areFirstAndLastTwoCharactersTheSame("ABCD"));
		assertFalse(helper.areFirstAndLastTwoCharactersTheSame("ABCD"));
	}
	
	@Test
	public void testAreFirstAndLastTwoCharactersTheSame_Basic_Positive() {
		//assertEquals(true, helper.areFirstAndLastTwoCharactersTheSame("ABAB"));
		assertTrue(helper.areFirstAndLastTwoCharactersTheSame("ABAB"));
	}
	
	// AB => true, A => false ?

}
