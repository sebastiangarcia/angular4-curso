package cl.wildit.junit.rest;

import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;

public class RestApiTest {

	@Test
	public void testEndpointServer_StatusCode_OK()
	  throws ClientProtocolException, IOException {
	  
	   // Given
	   HttpUriRequest request = new HttpGet( "https://demo4869696.mockable.io/servers" );
	 
	   // When
	   HttpResponse httpResponse = HttpClientBuilder.create().build().execute( request );
	 
	   // Then
	   assertEquals(
	     httpResponse.getStatusLine().getStatusCode(),
	     (HttpStatus.SC_OK));
	   
	}
	
	@Test
	public void
	givenRequestWithNoAcceptHeader_whenRequestIsExecuted_thenDefaultResponseContentTypeIsJson()
	  throws ClientProtocolException, IOException {
	  
	   // Given
	   String jsonMimeType = "application/json";
	   HttpUriRequest request = new HttpGet( "https://demo4869696.mockable.io/servers" );
	 
	   // When
	   HttpResponse response = HttpClientBuilder.create().build().execute( request );
	 
	   // Then
	   String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
	   assertEquals( jsonMimeType, mimeType );
	}
	
	/*@Test
	public void
	  givenUserExists_whenUserInformationIsRetrieved_thenRetrievedResourceIsCorrect()
	  throws ClientProtocolException, IOException {
	  
	    // Given
	    HttpUriRequest request = new HttpGet( "https://demo4869696.mockable.io/servers" );
	 
	    // When
	    HttpResponse response = HttpClientBuilder.create().build().execute( request );
	 
	    // Then
	    Servers resource = RetrieveUtil.retrieveResourceFromResponse(
	      response, Servers.class);
	    assertThat( "eugenp", Matchers.is( resource.getLogin() ) );
	}*/

}
